from projects.views import project_list
from django.urls import path



urlpatterns = [
    path("", project_list, name="list_projects"),
]
# first param needs to be " " and last param needs to be "list_projects"
# if name= does not match redirect in "tracker" folder error will raise NoReverseMatch