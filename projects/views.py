from django.shortcuts import render
from projects.models import Project
from django.contrib.auth.decorators import login_required

@login_required
def project_list(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "projects": projects,  # This was "projects" and it worked
    }
    return render(request, "projects/list.html", context)